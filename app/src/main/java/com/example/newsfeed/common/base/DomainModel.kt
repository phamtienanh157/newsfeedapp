package com.linh.mvvmdemo.common

import com.example.newsfeed.common.base.dto.LocalDto
import com.example.newsfeed.common.base.dto.RemoteDto

interface  DomainModel {
    fun toLocalDto() : LocalDto
    fun toRemoteDto() : RemoteDto
}