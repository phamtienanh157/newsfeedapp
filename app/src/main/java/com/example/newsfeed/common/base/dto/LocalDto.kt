package com.example.newsfeed.common.base.dto

import com.linh.mvvmdemo.common.DomainModel

interface LocalDto {
    fun mapToDomainModel(): DomainModel
    fun mapToRemoteDto(): RemoteDto
}