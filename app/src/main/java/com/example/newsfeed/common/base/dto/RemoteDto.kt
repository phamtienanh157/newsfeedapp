package com.example.newsfeed.common.base.dto

import com.linh.mvvmdemo.common.DomainModel

interface RemoteDto {
    fun mapToDomainModel() : DomainModel
    fun mapToLocalDto(): LocalDto
}