package com.example.newsfeed.common.base.dto

import com.linh.mvvmdemo.common.DomainModel

interface Dto {
    fun mapToDomainModel(): DomainModel
}