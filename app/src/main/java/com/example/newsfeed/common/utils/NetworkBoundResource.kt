package com.example.newsfeed.common.utils

import android.util.Log
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aibles.common.utils.networkexception.*
import com.example.newsfeed.common.utils.*
import kotlinx.coroutines.*
import kotlinx.coroutines.GlobalScope.coroutineContext
import retrofit2.Response
import timber.log.Timber
import java.net.SocketTimeoutException

abstract class NetworkBoundResource<ResultType, RequestType> {

    private val result = MutableLiveData<Resource<ResultType>>()
    private val supervisorJob = SupervisorJob()

    suspend fun build(): NetworkBoundResource<ResultType, RequestType> {
        withContext(Dispatchers.Main) {
            result.value =
                Resource.loading(null)
        }
        CoroutineScope(coroutineContext).launch(supervisorJob) {
            val dbResult = loadFromDb()
            if (shouldFetch(dbResult)) {
                try {
                    fetchFromNetwork(dbResult)
                } catch (exception: SocketTimeoutException) {
                    Timber.e(exception)
                    result.value =
                        Resource.error(RequestTimeoutException(exception.message), loadFromDb())
                } catch (e: Exception) {
                    Timber.e("An error happened: $e")
                    setValue(Resource.error(e, loadFromDb()))
                }
            } else {
                Timber.d("Return data from local database")
                setValue(Resource.success(dbResult))
            }
        }
        return this
    }

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    // ---

    private suspend fun fetchFromNetwork(dbResult: ResultType?) {
        Log.d(NetworkBoundResource::class.java.name, "Fetch data from network")
        setValue(Resource.loading(dbResult)) // Dispatch latest value quickly (UX purpose)
        val apiResponse = createCallAsync()
        if (apiResponse.isSuccessful) {
            Timber.e("Data fetched from network")
//            saveCallResults(processResponse(apiResponse))
            setValue(Resource.success(loadFromDb()))
        } else {
            setValue(handleNetworkError(apiResponse))
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        Timber.d("Resource: $newValue")
        if (result.value != newValue) result.postValue(newValue)
    }

    @WorkerThread
    private suspend fun handleNetworkError(response: Response<RequestType>): Resource<ResultType> {
        val errorBody = response.errorBody().toString()
        Timber.e(errorBody)
        val exception = when (response.code()) {
            400 -> BadRequestException(errorBody)
            401 -> NetworkAuthenticationException(errorBody)
            404 -> NetworkResourceNotFoundException(errorBody)
            500 -> NetworkServerException(errorBody)
            else -> Exception(errorBody)
        }

        return Resource.error(exception, loadFromDb())
    }

    @WorkerThread
    protected abstract fun processResponse(response: Response<RequestType>): ResultType

//    @WorkerThread
//    protected abstract suspend fun saveCallResults(items: ResultType)

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract suspend fun loadFromDb(): ResultType?

    @MainThread
    protected abstract suspend fun createCallAsync(): Response<RequestType>
}