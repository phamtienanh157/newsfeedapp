package com.aibles.common.utils.networkexception

class NetworkAuthenticationException(message: String?) : Exception(message)
class NetworkServerException(message: String?) : Exception(message)
class NetworkResourceNotFoundException(message: String?) : Exception(message)
class RequestTimeoutException(message: String?) : Exception(message)
class BadRequestException(message: String?) : Exception(message)