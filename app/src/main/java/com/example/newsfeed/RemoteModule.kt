package com.example.newsfeed

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun createRemoteModule(baseUrl: String) = module {

    factory {
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
    }

    factory {
        StethoInterceptor()
    }

//    factory {
//        HeaderAuthorizationInterceptor()
//    }

    factory {
        OkHttpClient.Builder()
            .addInterceptor(get<StethoInterceptor>())
            .addInterceptor(get<HttpLoggingInterceptor>())
//            .addInterceptor(get<HeaderAuthorizationInterceptor>())
            .build()
    }

    factory {
        Gson()
            .newBuilder()
            .create()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

}