package com.example.newsfeed.data.di

import androidx.room.Room
import com.example.newsfeed.data.local.PostDatabase
import com.example.newsfeed.data.remote.PostDataSource
import com.example.newsfeed.data.remote.PostService
import com.example.newsfeed.data.repository.PostRepositoryImpl
import com.example.newsfeed.domain.repository.PostRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit


val postDataModule = module {
    factory { get<Retrofit>().create(PostService::class.java) }
    factory { PostDataSource(get()) }

    factory { PostRepositoryImpl(get(), get()) as PostRepository}

    single {
        Room.databaseBuilder(androidContext(), PostDatabase::class.java, "post.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    factory { (get() as PostDatabase).postDao() }

}