package com.example.newsfeed.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.newsfeed.data.local.dao.PostDao
import com.example.newsfeed.data.local.entity.PostLocal
import com.example.newsfeed.data.remote.PostDataSource

@Database(entities = [PostLocal::class], version = 1)
abstract class PostDatabase : RoomDatabase(){
    abstract fun postDao(): PostDao

    companion object{
        @Volatile
        private var INSTANCE: PostDatabase? = null

        fun getDatabase(context: Context): PostDatabase {
            return INSTANCE
                ?: synchronized(this){
                val instance = Room.databaseBuilder(context.applicationContext, PostDatabase::class.java,
                    DATABASE_NAME
                ).build()
                INSTANCE = instance
                instance
            }
        }

        private const val DATABASE_NAME = "post_database"
    }
}