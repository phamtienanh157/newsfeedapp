package com.example.newsfeed.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.newsfeed.common.base.dto.LocalDto
import com.example.newsfeed.common.base.dto.RemoteDto
import com.example.newsfeed.domain.entity.Post

@Entity(tableName = "post")
class PostLocal(
    @PrimaryKey
    @ColumnInfo(name = "post_id")
    val id: Int,
    val avatar: String?,
    val name: String?,
    val time: String?,
    val content: String?,
    val photo: String?
) : LocalDto {
    override fun mapToDomainModel() = Post(id,avatar, name, time, content, photo)

    override fun mapToRemoteDto(): RemoteDto {
        TODO("Not yet implemented")
    }

}