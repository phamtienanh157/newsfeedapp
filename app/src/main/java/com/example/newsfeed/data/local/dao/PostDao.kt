package com.example.newsfeed.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.newsfeed.data.local.entity.PostLocal
import com.example.newsfeed.domain.entity.Post

@Dao
abstract class PostDao {
    @Query("SELECT * FROM post")
    abstract fun getPosts(): List<PostLocal>

    @Query("SELECT * FROM post WHERE post_id=:id")
    abstract fun getPostById(id:Int): PostLocal

    @Insert
    abstract suspend fun insertPost(post: PostLocal)
}