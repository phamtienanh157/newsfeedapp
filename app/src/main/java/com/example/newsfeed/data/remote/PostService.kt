package com.example.newsfeed.data.remote

import com.example.newsfeed.domain.entity.dto.PostDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface PostService {
//    @GET("posts/users/{id}")
//    suspend fun getPost(
//        @Path("id") id: Int
//    ) : Response<PostDto>

    @GET("posts")
    suspend fun getPosts() : Response<List<PostDto>>
}