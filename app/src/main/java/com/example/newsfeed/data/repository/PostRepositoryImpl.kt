package com.example.newsfeed.data.repository

import androidx.lifecycle.LiveData
import com.example.newsfeed.common.utils.NetworkBoundResource
import com.example.newsfeed.common.utils.Resource
import com.example.newsfeed.data.local.dao.PostDao
import com.example.newsfeed.data.remote.PostDataSource
import com.example.newsfeed.domain.entity.Post
import com.example.newsfeed.domain.entity.dto.PostDto
import com.example.newsfeed.domain.repository.PostRepository
import com.linh.mvvmdemo.common.DomainModel
import retrofit2.Response
import timber.log.Timber

class PostRepositoryImpl(private val postDao: PostDao, private val dataSource: PostDataSource) : PostRepository {

    override suspend fun getPosts(): LiveData<Resource<List<Post>>> {
        return object: NetworkBoundResource<List<Post>,List<PostDto>>(){

            override fun processResponse(response: Response<List<PostDto>>): List<Post> {
                return response.body()!!.map { it.mapToDomainModel() }
            }

            override fun shouldFetch(data: List<Post>?): Boolean {
                if (data == null){
                    Timber.d("No data")
                }
                return true
            }

            override suspend fun loadFromDb(): List<Post> {
                val postLocal= postDao.getPosts()
                return postLocal.map { it.mapToDomainModel() }
            }

            override suspend fun createCallAsync(): Response<List<PostDto>> {
                return dataSource.getPosts()
            }

//            override suspend fun saveCallResults(items: List<Post>) {
//                postDao.insertPost(items.map { it.toLocalDto() })
//            }

        }.build().asLiveData()
    }

    override suspend fun insertPost(post: Post) {
        postDao.insertPost(post.toLocalDto())
    }
}