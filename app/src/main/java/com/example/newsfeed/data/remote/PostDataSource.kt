package com.example.newsfeed.data.remote

import com.example.newsfeed.domain.entity.dto.PostDto
import retrofit2.Response

class PostDataSource(private val service: PostService){
    suspend fun getPosts(): Response<List<PostDto>> = service.getPosts()
}