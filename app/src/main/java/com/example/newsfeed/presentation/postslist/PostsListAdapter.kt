package com.example.newsfeed.presentation.postslist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.newsfeed.databinding.PostRowBinding
import com.example.newsfeed.domain.entity.Post

class PostsListAdapter() : ListAdapter<Post, PostsListAdapter.PostViewHolder>(PostsListDiffUtil()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder = PostViewHolder.from(parent)

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val post = getItem(position)
        holder.bind(post)
    }
    class PostViewHolder(private val binding: PostRowBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(post: Post){
            binding.post = post
            binding.executePendingBindings()
        }
        companion object{
            fun from(parent: ViewGroup) : PostViewHolder{
                val inflater = LayoutInflater.from(parent.context)
                val binding = PostRowBinding.inflate(inflater,parent,false)
                return PostViewHolder(binding)
            }
        }
    }
}
class PostsListDiffUtil() : DiffUtil.ItemCallback<Post>() {
    override fun areContentsTheSame(oldItem: Post, newItem: Post) = oldItem == newItem
    override fun areItemsTheSame(oldItem: Post, newItem: Post) = oldItem.id == newItem.id
}