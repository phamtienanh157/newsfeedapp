package com.example.newsfeed.presentation.notification.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.newsfeed.R
import com.squareup.picasso.Picasso
import com.example.newsfeed.presentation.notification.PostNotification
import kotlinx.android.synthetic.main.notification_item.view.*

class PostNotiAdapter (private val postsNotification: ArrayList<PostNotification>, private val context: Context) :
    RecyclerView.Adapter<PostNotiAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.notification_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = postsNotification.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = postsNotification[position]
        holder.textView2.text = currentItem.time
        holder.textView1.text = currentItem.content
        Picasso.with(context).load(currentItem.photo).into(holder.imageView1)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView1: ImageView = itemView.avatar_notification
        val textView1: TextView = itemView.text_contentNotification
        val textView2: TextView = itemView.text_timeNotification


    }
}
