package com.example.newsfeed.presentation.notification

data class PostNotification(val photo: String, val content: String, val time: String)