package com.example.newsfeed.presentation.privacy

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.newsfeed.R
import kotlinx.android.synthetic.main.fragment_privacy.*

class PrivacyFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_privacy, container, false)
        return view
    }

//    @SuppressLint("CommitPrefEdits")
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        button_back_fragment_one_post.setOnClickListener {
//            findNavController().navigate(R.id.action_privateFragment_to_onePostFragment)
//        }
//        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
//        with (sharedPref.edit()) {
//            putInt("privacy", 0)
//            apply()
//        }
//        radiobutton_privacy_public.setOnClickListener {
//            with (sharedPref.edit()) {
//                putInt("privacy", 1)
//                apply()
//            }
//            findNavController().navigate(R.id.action_privateFragment_to_onePostFragment)
//        }
//        radiobutton_privacy_friend.setOnClickListener {
//            with (sharedPref.edit()) {
//                putInt("privacy", 2)
//                apply()
//            }
//            findNavController().navigate(R.id.action_privateFragment_to_onePostFragment)
//        }
//        radiobutton_privacy_block.setOnClickListener {
//            with (sharedPref.edit()) {
//                putInt("privacy", 3)
//                apply()
//            }
//            findNavController().navigate(R.id.action_privateFragment_to_onePostFragment)
//        }
//    }
}

