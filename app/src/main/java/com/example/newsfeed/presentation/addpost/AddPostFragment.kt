package com.example.newsfeed.presentation.addpost

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.newsfeed.R
import com.example.newsfeed.data.local.PostDatabase
import com.example.newsfeed.data.local.dao.PostDao
import com.example.newsfeed.data.remote.PostDataSource
import com.example.newsfeed.data.remote.PostService
import com.example.newsfeed.data.repository.PostRepositoryImpl
import com.example.newsfeed.databinding.FragmentAddPostBinding
import com.example.newsfeed.domain.repository.PostRepository
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.linh.mvvmdemo.common.base.BaseFragment
import com.linh.mvvmdemo.common.base.BaseViewModel
import com.mancj.slideup.SlideUp
import kotlinx.android.synthetic.main.fragment_add_post.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class  AddPostFragment : BaseFragment() {


    private val retrofit =  Retrofit.Builder()
        .client(OkHttpClient())
        .baseUrl("http://api.coxude.me/social/post-composite-service/api/v1/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
    private val service = retrofit.create(PostService::class.java)
    private val dataSource = PostDataSource(service)

    private val database = PostDatabase.getDatabase(requireContext())
    private val postDao = database.postDao()
    private val postRepository = PostRepositoryImpl(postDao,dataSource)

    private val viewModel: AddPostViewModel by viewModels{AddPostViewModelFactory(postRepository)}
    private lateinit var binding: FragmentAddPostBinding
    override fun getViewModel(): BaseViewModel = viewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentAddPostBinding.inflate(inflater,container,false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.listener = viewModel
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val public = sharedPref.getInt("privacy", 0)
        if (public == 1) {
            image_view_privacy.setImageResource(R.drawable.icon_privacy_public)
            textView_privacy.text = "Công khai"
        }
        if (public == 2) {
            image_view_privacy.setImageResource(R.drawable.icon_privacy_friend)
            textView_privacy.text = "Bạn bè"
        }
        if (public == 3) {
            image_view_privacy.setImageResource(R.drawable.icon_privacy_lock)
            textView_privacy.text = "Chỉ mình tôi"
        }

        val slideUp: SlideUp = SlideUp(bottom_sheet)
        slideUp.hideImmediately()
        btBottomSheetDialog.setOnClickListener(View.OnClickListener {
            slideUp.animateIn()
        })
    }
}