package com.example.newsfeed.presentation.postslist

import androidx.lifecycle.*
import com.example.newsfeed.common.utils.AppDispatchers
import com.example.newsfeed.common.utils.Resource
import com.example.newsfeed.domain.entity.Post
import com.example.newsfeed.domain.repository.PostRepository
import com.example.newsfeed.domain.usecase.GetPostUseCase
import com.linh.mvvmdemo.common.base.BaseViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.lang.IllegalArgumentException

class PostsListViewModel (private val dispatchers: AppDispatchers, private val getPostUseCase: GetPostUseCase) : BaseViewModel(), PostsListActionListener{

    private val _post = MediatorLiveData<Resource<List<Post>>>()
    val posts: LiveData<Resource<List<Post>>> get() = _post
    private var postSource: LiveData<Resource<List<Post>>> = MutableLiveData()

    init {
        getPosts()
    }
    private fun getPosts() {
        viewModelScope.launch(dispatchers.main) {
            _post.removeSource(postSource)
            withContext(dispatchers.io) {
                postSource = getPostUseCase()
            }
            try {
                _post.addSource(postSource) {
                    Timber.d("getAllPost() $it")
                    _post.value = it
                }
            } catch (e: IllegalArgumentException) {
                Timber.e(e)
            }
        }
    }
    override fun onClickAdd() {
        navigate(PostsListFragmentDirections.actionPostListFragmentToAddPostFragment())
    }
}
@Suppress("UNCHECKED_CAST")
class PostsListViewModelFactory(private val postRepository: PostRepository): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GetPostUseCase(postRepository) as T
    }

}