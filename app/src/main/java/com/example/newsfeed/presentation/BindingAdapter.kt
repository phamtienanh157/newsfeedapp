package com.example.newsfeed.presentation

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsfeed.common.utils.Resource
import com.example.newsfeed.domain.entity.Post
import com.example.newsfeed.presentation.postslist.PostsListAdapter


@BindingAdapter("app:posts")
fun setPostList(recyclerView: RecyclerView, posts: Resource<List<Post>>?) {
    if (recyclerView.adapter!= null && posts != null) {
        (recyclerView.adapter as? PostsListAdapter)?.submitList(posts.data)
    }
}
@BindingAdapter("android:src")
fun setPhoto(imageView: ImageView?, url: String?){
    if(imageView!= null && url != null){
        Glide.with(imageView).load(url).into(imageView)
    }
}
@BindingAdapter("android:text")
fun setText(textView : TextView?, text : String?){
    if(textView!= null && text != null){
        textView.text = text
    }
}