package com.example.newsfeed.presentation.postslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsfeed.data.local.PostDatabase
import com.example.newsfeed.data.remote.PostDataSource
import com.example.newsfeed.data.remote.PostService
import com.example.newsfeed.data.repository.PostRepositoryImpl
import com.example.newsfeed.databinding.FragmentPostsListBinding
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.linh.mvvmdemo.common.base.BaseFragment
import com.linh.mvvmdemo.common.base.BaseViewModel
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PostsListFragment : BaseFragment() {


    private val retrofit = Retrofit.Builder()
        .client(OkHttpClient())
        .baseUrl("http://api.coxude.me/social/post-composite-service/api/v1/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
    private val service = retrofit.create(PostService::class.java)
    private val dataSource = PostDataSource(service)

    private val database = PostDatabase.getDatabase(requireContext())
    private val postDao = database.postDao()
    private val postRepository = PostRepositoryImpl(postDao, dataSource)

    private val viewModel: PostsListViewModel by viewModels {
        PostsListViewModelFactory(
            postRepository
        )
    }

    private lateinit var binding: FragmentPostsListBinding

    override fun getViewModel(): BaseViewModel = viewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPostsListBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        binding.listener = viewModel
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.recyclerViewPostList) {
            adapter = PostsListAdapter()
            layoutManager = LinearLayoutManager(requireContext())
        }
    }
}