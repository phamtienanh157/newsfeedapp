package com.example.newsfeed.presentation.addpost

interface AddPostActionListener {
    fun onClickPost()
    fun onClickBack()
}