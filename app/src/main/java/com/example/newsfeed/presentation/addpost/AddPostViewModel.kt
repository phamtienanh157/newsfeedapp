package com.example.newsfeed.presentation.addpost

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.newsfeed.domain.entity.Post
import com.example.newsfeed.domain.repository.PostRepository
import com.linh.mvvmdemo.common.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddPostViewModel(private val postRepository: PostRepository) : BaseViewModel(),
    AddPostActionListener {

    val content = MutableLiveData<String>()
    val name = ""
    val avatar = ""
    val photo = "https://picsum.photos/1000/900?random&"
    override fun onClickPost() {
//        savePost()
    }

//    private fun savePost() {
//        viewModelScope.launch {
//            withContext(Dispatchers.IO) {
//                val postContent = content.value
//                val post = Post(
//                    id,
//                     avatar,
//                    name,
//                    "10:15 AM",
//                    postContent ?: "",
//                   photo
//                )
//                postRepository.insertPost(post)
//            }
//            navigateBack()
//        }
//    }

    override fun onClickBack() {
        navigateBack()
    }

}

@Suppress("UNCHECKED_CAST")
class AddPostViewModelFactory(private val postRepository: PostRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddPostViewModel(postRepository) as T
    }
}