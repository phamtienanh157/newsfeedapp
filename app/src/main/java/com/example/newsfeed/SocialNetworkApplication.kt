package com.example.newsfeed

import android.app.Application
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class SocialNetworkApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin()
        initTimber()
        Stetho.initializeWithDefaults(this)
//
//        Hawk.init(this).build()
    }
    private val BASE_URL = "http://api.coxude.me/social/post-composite-service/api/v1/"
    private fun initKoin() {
        startKoin {
            androidContext(this@SocialNetworkApplication)
            val modules = modules(
                    createRemoteModule(BASE_URL)
            )
        }
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}