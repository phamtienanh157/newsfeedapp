package com.example.newsfeed.domain.usecase

import androidx.lifecycle.LiveData
import com.example.newsfeed.common.utils.Resource
import com.example.newsfeed.domain.entity.Post
import com.example.newsfeed.domain.repository.PostRepository

class GetPostUseCase(private val postRepository: PostRepository) {
    suspend operator fun invoke() = postRepository.getPosts()
}