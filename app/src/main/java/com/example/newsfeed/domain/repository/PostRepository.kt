package com.example.newsfeed.domain.repository

import androidx.lifecycle.LiveData
import com.example.newsfeed.common.utils.Resource
import com.example.newsfeed.domain.entity.Post

interface PostRepository {
//    suspend fun getAllPosts() : LiveData<Resource<List<Post>>>
    suspend fun getPosts(): LiveData<Resource<List<Post>>>
    suspend fun insertPost(post: Post)
}