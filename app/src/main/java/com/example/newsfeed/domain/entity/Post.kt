package com.example.newsfeed.domain.entity

import com.example.newsfeed.data.local.entity.PostLocal
import com.example.newsfeed.domain.entity.dto.PostDto
import com.linh.mvvmdemo.common.DomainModel


data class Post(
    val id: Int,
    val avatar: String?,
    val name: String?,
    val time: String?,
    val content: String?,
    val photo: String?
) : DomainModel {
    override fun toLocalDto() = PostLocal(id,avatar, name, time, content, photo)

    override fun toRemoteDto() = PostDto(id,avatar, name, time, content, photo)

}