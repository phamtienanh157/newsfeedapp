package com.example.newsfeed.domain.entity.dto

import com.example.newsfeed.common.base.dto.LocalDto
import com.example.newsfeed.common.base.dto.RemoteDto
import com.example.newsfeed.data.local.entity.PostLocal
import com.example.newsfeed.domain.entity.Post
import com.google.gson.annotations.SerializedName
import com.linh.mvvmdemo.common.DomainModel

data class PostDto(
    @SerializedName("uid")
    val id: Int,
    val avatar: String? = null,
    @SerializedName("created_by")
    val name: String? = null,
    @SerializedName("created_date")
    val time: String? = null,
    @SerializedName("caption")
    val content: String? = null,
    @SerializedName("images")
    val photo: String? = null
) : RemoteDto {
    override fun mapToDomainModel()= Post(id, avatar ?: "", name ?: "", time ?: "", content ?: "", photo ?: "")

    override fun mapToLocalDto() = PostLocal(id,avatar ?:"",name?: "",time?: "",content?: "",photo?: "")

}